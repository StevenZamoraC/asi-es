package practica1.josesanchez.facci.serviciosautomovil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MANTENIMIENTO extends AppCompatActivity {
Button modelo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mantenimiento);
        modelo= (Button) findViewById(R.id.buttonModelo);
        modelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // se genera la navegabilidad entre la actividad principal y la actividad de login
                Intent intent = new Intent(MANTENIMIENTO.this, MODELOAUTO.class);
                startActivity(intent);
            } });
    }
}
